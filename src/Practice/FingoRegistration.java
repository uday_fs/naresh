package Practice;

import java.awt.RenderingHints.Key;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

// Commented by me for git code

// Commited for develop

public class FingoRegistration {

	static WebDriver d=new FirefoxDriver();
	static String passwordL="//div[@id='checkout-step-login']/div/div/form/fieldset/ul/li[2]/div/input";
	static String firstnameL="//div[@class='account-create']/form/div/ul/li/div/div/input";
	
	
	public static boolean isElementPresent(WebDriver driver, String xpath ) {
		   try {
			   driver.findElement(By.xpath(xpath));
			   return true;
		   }
		   catch(Exception e){
			   return false;
		   }
	}
	public static String verifymailRegistration(WebDriver driver,String passwordLocator,String nameLocator) {
		if(isElementPresent(driver,passwordLocator)) {
			return "registered";
		}else if (isElementPresent(driver,nameLocator)) {
			return "notregistered";
		}else {
			return "notvalid";
		}
	}
	
	public static void verifymail(WebDriver driver,String mail,String passwordLocator,String firstNameLocator) {
		
		 WebElement userName = d.findElement(By.xpath("//div[@id='checkout-step-login']/div/div/form/fieldset/ul/li/div/input"));
		 WebElement errorNull = d.findElement(By.xpath("//div[@id='advice-required-entry-login-email']"));
		 WebElement continueButton = d.findElement(By.xpath("//div[@id='chk-loginbtns']/div/button[1]"));
		 WebElement errorInvalid = d.findElement(By.xpath("//div[@id='advice-validate-email-login-email']"));
		 WebElement password = d.findElement(By.xpath("//div[@id='checkout-step-login']/div/div/form/fieldset/ul/li[2]/div/input"));
		 WebElement firstName = d.findElement(By.xpath("//div[@class='account-create']/form/div/ul/li/div/div/input"));
		userName.sendKeys(mail);
		continueButton.click();
		if (mail.isEmpty()||mail.equalsIgnoreCase("")) {
			Assert.assertEquals(errorNull.getText(), "This is a required field.");
			System.out.println("************Null Success********");
		} else {
			String result=verifymailRegistration(driver,passwordLocator,firstNameLocator);
			if (result.equalsIgnoreCase("notvalid")) {
				Assert.assertEquals(errorInvalid.getText(),"Please enter a valid email address. For example johndoe@domain.com.");
				System.out.println("************ Invalid Success********");
			} else {
				System.out.println("************ VALID Success********");
			}
		} 
	}
	
	
	public static void main(String[] args) throws InterruptedException {
	
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		d.get("https://stage.fingoshop.com/");
		String mainWindow = d.getWindowHandle();
		d.findElement(By.xpath("//div[@class='form-search']/input")).sendKeys("test");
		Actions a=new Actions(d);
		a.sendKeys(Keys.ENTER).perform();
		
		List<WebElement> product_List = d.findElements(By.xpath("//div[@class='products-grid']/ul/li"));
		product_List.size();
		//System.out.println(product_List.size());
		for (WebElement product : product_List) {
			if (product.findElement(By.className("product-name")).
					getText().equalsIgnoreCase("Test Product 1...")) {
				product.click();
				break;
			}
		}
		Set<String> allWindows = d.getWindowHandles();
		for (String window : allWindows) {
			if (!window.equals(mainWindow)) {
				d.switchTo().window(window);
				//d.getCurrentUrl();
				//System.out.println(d.getCurrentUrl());
				//BuyNow Button -------------------------------------------------------------
				d.findElement(By.xpath("//div[@class='add-to-cart']/div[2]/button[4]")).click();
				Thread.sleep(2000);
				//System.out.println(d.getCurrentUrl()+"----"+d.getTitle());
				// Email id in Checkout -----------------------------------------------------
				verifymail(d,"ctouch.test3@gmail.com",passwordL,firstnameL);
			
			}
		}
		
		
	
	}
	
	
	

}
