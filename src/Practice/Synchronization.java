package Practice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Synchronization {

	public static void main(String[] args) {
		WebDriver d=new FirefoxDriver();
		d.get("https://www.ifsccodebank.com/");
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(3600, TimeUnit.NANOSECONDS);
		Select bank=new Select(d.findElement(By.xpath("//div[@class='ContentPage']/div[7]/div/table/tbody/tr/td[2]/select")));
		bank.selectByIndex(2);
		
		WebDriverWait wait=new WebDriverWait(d,60);
		wait.until(ExpectedConditions.elementToBeSelected(d.findElement(By.xpath("//div[@class='ContentPage']/div[7]/div/table/tbody/tr[2]/td[2]"))));
		Select bank1=new Select(d.findElement(By.xpath("//div[@class='ContentPage']/div[7]/div/table/tbody/tr[2]/td[2]/select")));
		bank1.selectByIndex(2);		
		
	}

}
