package Practice;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import com.sun.jna.platform.FileUtils;

public class WindowhandelExample {
			
			static WebDriver driver = new FirefoxDriver();
			static int count=1;
	public static void main(String[] args) throws IOException {
			
			String link1="https://www.ecatering.irctc.co.in/";
			String link2="https://www.air.irctc.co.in/";
			String link3="https://www.irctc.co.in/eticketing/alertStaticMsg.jsf";

			
			driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			String window = driver.getWindowHandle();
			Actions a=new Actions(driver);
			a.moveToElement(driver.findElement(By.xpath("/html/body/div/div/div[8]/div/div/ul/li[1]/a"))).build().perform();
			driver.findElement(By.linkText("E-Catering")).click();
			
			driver.findElement(By.linkText("Flights")).click();
			driver.findElement(By.linkText("Alerts & Updates")).click();
			
			Set<String> windowHandles = driver.getWindowHandles();
			for (String string : windowHandles) {
				if (string.equalsIgnoreCase(window)) {
					System.out.println("soso");
				}else {
					driver.switchTo().window(string);
					//if (driver.getCurrentUrl().equals(link1)) {
						takeScreens();
						count++;
						//}
					
				}
			}
	}
	
	
	public static void takeScreens() throws IOException {
		
		File screenshotAs = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		org.apache.commons.io.FileUtils.copyFile(screenshotAs, new File("C:\\Users\\uday\\Desktop\\Screenshots\\"+"files"+count+".png"));
		
		
	}

}
