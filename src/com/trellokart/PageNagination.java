package com.trellokart;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PageNagination {

		static WebDriver d=new FirefoxDriver();
	public static void main(String[] args) {
		
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		d.get("https://www.fingoshop.com/men/watches/mycross-watches");
		
		for (int i = 0; i < 100; i++) {
			int event = clickNextButton();
			System.out.println(d.getCurrentUrl());
			if (event==1) {
				break;
			}
		}
		
		d.close();
	}
	
	public static int clickNextButton() {
		try {
			WebElement findElement = d.findElement(By.cssSelector(".next.i-next"));
			((JavascriptExecutor)d).executeScript("arguments[0].click();",findElement);
			return 0;
		} catch (Exception e) {
			System.err.println("Element is not there");
			return 1;
			//e.printStackTrace();
		}		

	}
}
