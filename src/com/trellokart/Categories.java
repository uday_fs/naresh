package com.trellokart;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.jetty.html.Break;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Categories {

	static WebDriver d= new FirefoxDriver();
	
	public static void main(String[] args) throws InterruptedException {

		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		
		d.get("https://www.fingoshop.com/");
		Actions a=new Actions(d);
		a.moveToElement(d.findElement(By.xpath("//div[@class='cdz-main-menu']/div/ul/li[1]/a"))).build().perform();
		List<WebElement> maindivs = d.findElements(By.xpath("//div[@class='cdz-main-menu']/div/ul/li[1]/ul/li/div/div/div"));
		for (WebElement maindiv : maindivs) {
			findClickLinks(maindiv);
			break;
		}
	}

	
	public static void findClickLinks(WebElement e) throws InterruptedException {
		
		List<WebElement> links = e.findElements(By.tagName("a"));
		for (WebElement link : links) {
			System.out.println(link.getAttribute("href"));
			clickURL(link);
//			Thread.sleep(10000);
//			d.navigate().to("https://www.fingoshop.com/");
//			Actions a=new Actions(d);
//			a.moveToElement(d.findElement(By.xpath("//div[@class='cdz-main-menu']/div/ul/li[1]/a"))).build().perform();
			break;
		}
		
		
	}
	
	
	public static void clickURL(WebElement e) {
		e.click();
		System.out.println("Came here");
	}
	
}
