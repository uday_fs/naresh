package com.trellokart;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class RandomSelection {

	 static WebDriver d= new FirefoxDriver();
	
	 public static void main(String[] args) {
		
		
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		d.get("https://www.trellokart.com/");
		
		// Selecting a category randomly 
		
		Actions a=new Actions(d);
		WebElement mobileAccessories = d.findElement(By.xpath("//nav[@class='navigation custommenu']/ul/li[2]"));
		a.moveToElement(mobileAccessories).build().perform();
		WebElement allListParent = d.findElement(By.cssSelector(".subchildmenu.col-sm-12.mega-columns.columns4"));
		List<WebElement> allList = allListParent.findElements(By.tagName("li"));		
		Random r=new Random();
		int nextInt = r.nextInt(allList.size());
		allList.get(nextInt).click();
		
		String categorywindow = d.getWindowHandle();
		// Selecting a product randomly
		if (verifyProducts()) {
			WebElement productList = d.findElement(By.cssSelector(".products.wrapper.grid.products-grid"));
			List<WebElement> products = productList.findElements(By.tagName("li"));
			Random rp=new Random();
			int nextInt2 = rp.nextInt(products.size());
			products.get(nextInt2).findElement(By.xpath("//div[@class='product-info']/div/div/a")).click();
		
		// Switching to product page	
			Set<String> windowHandles = d.getWindowHandles();
			for (String single : windowHandles) {
				if (!single.equals(categorywindow)) {
					d.switchTo().window(single);
					if (verifyStock()) {
						//Click on Buy now Button
						d.findElement(By.className("buynow-button")).click();
					}else {
						System.out.println("Product was in out-ff-stock");
					}
				}
			}
		}else {
			System.out.println("Products were not there in the opened category");
		}
	}

	
/*
 *  Verify whether the opened category is having products or not
 *  	
 */
	public static boolean verifyProducts() {
		WebElement products = d.findElement(By.xpath("//div[@id='layered-ajax-list-products']/div"));
		if (products.getText().equalsIgnoreCase("We can't find products matching the selection.")) {
			System.out.println("There no products in the selected category");
			return false;
		}else {
			return true;
		}
	}


	/*
	 * Verify stock status	 
	 */
	public static boolean verifyStock() {
		
		String text = d.findElement(By.xpath("//div[@class='product-info-price']/div/div/span")).getText();
		if (text.equalsIgnoreCase("In stock")) {
			return true;
		} else {
			return false;
		}
	}


}
