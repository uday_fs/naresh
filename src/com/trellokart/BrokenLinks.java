package com.trellokart;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class BrokenLinks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver d =new FirefoxDriver();
		//int count=1;
		d.get("https://www.trellokart.com/");
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		Actions a =new Actions(d);
		WebElement mobileAccessories = d.findElement(By.xpath("//nav[@class='navigation custommenu']/ul/li[2]"));
		a.moveToElement(mobileAccessories).build().perform();
		WebElement allListParent = d.findElement(By.cssSelector(".subchildmenu.col-sm-12.mega-columns.columns4"));
		List<WebElement> allList = allListParent.findElements(By.tagName("li"));
		System.out.println(allList.size());
		for (WebElement li : allList) {
			String text = li.findElement(By.tagName("a")).getAttribute("href");
			//System.out.println(count+"--"+text);
			//count++;
			verifyLinkActive(text);
		}
	}
	
	public static void verifyLinkActive(String linkUrl)
	{
        try 
        {
           URL url = new URL(linkUrl);
           
           HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
           
           httpURLConnect.setConnectTimeout(3000);
           
           httpURLConnect.connect();
           
           if(httpURLConnect.getResponseCode()==200)
           {
               System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage());
            }
          if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)  
           {
               System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
            }
        } catch (Exception e) {
           
        }
    } 

}







